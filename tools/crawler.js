const cheerio = require('cheerio');
const fetch = require('node-fetch');
const validUrl = require('valid-url');

function crawlSite(req, res) {
    console.log(`Crawling ${req.body.url}`);
    fetch(req.body.url)
        .then(resp => resp.text())
        .then(html => {
            let $ = cheerio.load(html);
            let links = $('a');
            let validLinks = [];
            $(links).each((index, linkData) => {
                let link = linkData.attribs.href;
                if (validUrl.isUri(link) && link !== req.body.url) {
                    console.log(`Found: ${link}`);
                    validLinks.push(link);
                }
            });
            res.send(validLinks);
        })
        .catch(err => {
            if (err.errno ==='ENOTFOUND') {
                console.log(`Server could not connect to ${req.body.url}`);
                res.statusMessage = `Server could not connect to ${req.body.url}`;
                res.status(400).end();
            } else {
                console.log(err);
                res.statusMessage = 'Something went wrong';
                res.status(520).end();
            }
        })
}

module.exports = {crawlSite};