const devices = require('puppeteer/DeviceDescriptors');
const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');


/**
 * Sends a screenshot to the client in png dataURL form
 *
 * @param req
 * @param res
 * @param next
 */
function sendWebScreenshot(req, res, next) {
    generateWebScreenshot(req)
        .then(() => {
            let screenshotData = req.body;

            let screenshotPath = `${__dirname}/../screenshots/${screenshotData.fileName}.png`;
            fs.readFile(screenshotPath, {encoding: 'base64'}, (err, data) => {
                let base64screenshot = 'data:image/png;base64,' + data;
                let screenshotJson = {"deviceName": screenshotData.deviceName, "deviceType": screenshotData.deviceType, "screenshot": base64screenshot};
                res.json(screenshotJson);
            });

            console.log('Image Sent!');
        })
        .catch(err => {
            if (err.message.slice(5, 26) === 'ERR_NAME_NOT_RESOLVED') {
                console.error('Invalid Website');
                res.statusMessage = 'Invalid Website';
                res.status(400).end();
            } else {
                console.log(err);
            }
        })
}

/**
 * Takes a screenshot of a given website and saves it to ./screenshots/
 *
 * @param req
 * @returns {Promise<string>}
 */
async function generateWebScreenshot(req) {
    let url = req.query.url;
    let deviceName = req.body.deviceName;
    let resolution = req.body.resolution;
    let screenshotName = req.body.fileName;

    await generateScreenshot(url, deviceName, screenshotName, resolution);
}

function sendCliScreenshot(req, res) {
    console.log('started');
    generateCliScreenshot(req)
        .then(() => {
            let screenshotPath = path.resolve(`${__dirname}/../screenshots/cliScreenshot.png`);
            res.sendFile(screenshotPath)
        })
}

async function generateCliScreenshot(req) {
    let url = req.query.url;
    let deviceName = req.query.deviceName;

    await generateScreenshot(url, deviceName, 'cliScreenshot');
}

async function generateScreenshot(url, deviceName, screenshotName, resolution = {width: 1920, height: 1080}) {
    let browser = await puppeteer.launch({headless: true, ignoreDefaultArgs: ['--disable-extensions']});
    let page = await browser.newPage();

    if (deviceName in devices) {
        console.log(`Emulating ${deviceName}`);
        let device = devices[deviceName];
        await page.emulate(device);
    } else {
        await page.setViewport(resolution);
    }

    console.log(`Screenshotting Website: ${url}`);
    console.log(`Device: ${deviceName}`);

    await page.goto(`${url}`);
    await page.waitFor(0);
    await page.screenshot({path: `./screenshots/${screenshotName}.png`, fullPage: true});

    console.log('screenshot taken');
    browser.close();
}

module.exports = {
    sendWebScreenshot: sendWebScreenshot,
    sendCliScreenshot: sendCliScreenshot
};