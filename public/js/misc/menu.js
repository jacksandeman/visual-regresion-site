const menu = document.querySelector('.menu');
const menuButton = document.querySelector('.menu-button');
const exitMenuButton = document.querySelector('.menu-exit-button');

menuButton.addEventListener('click', () => {
    menu.style.display = 'flex';
});

exitMenuButton.addEventListener('click', () => {
    menu.style.display = 'none';
});