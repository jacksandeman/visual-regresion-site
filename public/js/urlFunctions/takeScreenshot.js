import resolutions from '../../resolutionsJson.js'
import {screenshotInspector} from '../misc/screenshotInspector.js';
import {disableButtonCheck} from "../screenshotOptions/urlList.js";

screenshotButton.addEventListener('click', () => takeScreenshot(getURL()));

/**
 * Displays screenshots
 *
 * @description
 * Adds an event listener to the screenshot button which makes a
 * requests to the server to take screenshots of the website
 * at specified resolutions. It then displays them in the area
 * underneath the site URL input box.
 *
 */
function takeScreenshot(inputUrl) {
    screenshotButton.disabled = true;
    takeScreenshotsButton.disabled = true;

    statusText.displayText(`Screenshotting: ${inputUrl}`);
    console.log(`Screenshotting: ${inputUrl}`);

    let resolutions = getResolutions();

    if (!validateURL(inputUrl)) {
        statusText.displayText('Please enter a valid URL');
        console.log(`Invalid URL: ${inputUrl}`);
        screenshotButton.disabled = false;
    } else if (resolutions.length === 0) {
        statusText.displayText('Please select at least one resolution');
        screenshotButton.disabled = false;
    } else {
        generateScreenshots(inputUrl, resolutions);
    }
}

function onUrlBarEnterPress(event) {
    if (event.keyCode === 13) {
        takeScreenshot(getURL());
    }

}
urlBar.addEventListener('keydown', onUrlBarEnterPress);

/**
 * @typedef {[{resolution: {width, height}, name: *}]} screenshotDataArray
 */

/**
 * returns the resolutions and their devices
 *
 * @description
 * Returns an array of Resolutions and their corresponding devices
 * according to what resolutions are checked
 *
 * @returns {screenshotDataArray} screenshotDataArray
 */
function getResolutions() {
    let resolutionElementArray = Array.from(resolutionList.children);
    let screenshotDataArray = [];

    resolutionElementArray.forEach(resolutionElement => {
        if (resolutionElement.firstChild.checked === true) {
            let screenshotData = getScreenshotData(resolutionElement);
            screenshotDataArray.push(screenshotData);
        }
    });

    return screenshotDataArray;
}

/**
 * Gets screenshot data from an <input> element
 *
 * Uses the key stored in the <input> elements name attribute to
 * get the data from the resolutionsJson.js file
 *
 * @param {Element} resolutionElement
 * @returns {{name: *, resolution: number | {width: number, height: number}}}
 */
function getScreenshotData(resolutionElement) {
    let key = resolutionElement.firstChild.name;

    let jsonData = resolutions[key];
    let resolution = jsonData.resolution;
    let deviceName = jsonData.deviceName;
    let deviceType = jsonData.deviceType;

    let fileName = generateFileName(key);

    return {
        "resolution": resolution,
        "deviceName": deviceName,
        "deviceType": deviceType,
        "fileName": 'screenshot' // TODO Change this to fileName for prod
    };
}

/**
 * Generates a file name
 *
 * @param key
 * @returns {string}
 */
function generateFileName(key) {
    let url = getURL();

    let a = document.createElement('a');
    a.href = url;

    return `${a.host}-${key}`;
}

/**
 * requests a screenshot for every resolution checked
 *
 * @param {String} inputUrl
 * @param {screenshotDataArray} screenshotData
 */
function generateScreenshots(inputUrl, screenshotData) {
    screenshotData.forEach(data => {
        generateScreenshot(inputUrl, data)
    })
}

/**
 * Requests a screenshot for a given URL and resolution
 * then displays the screenshot created
 *
 * @param {String} inputUrl
 * @param {screenshotDataArray} screenshotData
 */
function generateScreenshot(inputUrl, screenshotData) {
    console.log(screenshotData);

    fetch(`${window.origin}/take-web-screenshot?url=${inputUrl}`, createJsonPostData(screenshotData))
        .then(response => {
            if (!response.ok) throw Error(response.statusText);
            return response.json();
        })
        .then(screenshotData => {
            displayScreenshot(screenshotData);
            changeButtonStates();
        })
        .catch(err => {
            if (err.message === 'Invalid Website') {
                statusText.displayText(err.message);
            } else {
                statusText.displayText("Something went wrong check console");
                console.log(err);
            }
            screenshotButton.disabled = false;
        })
}

/**
 * Displays the screenshot along with its device name
 *
 * @param {screenshotDataArray} screenshotData
 */
function displayScreenshot(screenshotData) {
    let screenshotContainer = createImageContainer(screenshotData);
    screenshotCarousel.append(screenshotContainer);

    statusText.hideText();
}

function changeButtonStates() {
    screenshotButton.disabled = false;
    downloadButton.disabled = false;
    removeScreenshotsButton.disabled = false;
    disableButtonCheck();
}

/**
 * Creates the html that contains the screenshot and device name
 *
 * @param {screenshotDataArray} screenshotData
 * @returns {HTMLDivElement} screenshotContentContainer
 */
function createImageContainer(screenshotData) {

    let screenshotContentContainer = document.createElement('div');
    screenshotContentContainer.classList.add('screenshot-content');

    let screenshotResolutionName = document.createElement('h4');
    screenshotResolutionName.classList.add('screenshot-resolution-name');
    screenshotResolutionName.innerText = screenshotData.deviceName;

    let screenshotContainer = document.createElement('div');
    screenshotContainer.classList.add('screenshot-container');
    screenshotContainer.setAttribute('data-device-type', screenshotData.deviceType);

    let screenshot = document.createElement('img');
    screenshot.src = screenshotData.screenshot;
    screenshot.classList.add('screenshot');
    screenshot.addEventListener('click', enlargeScreenshot);

    screenshotContainer.append(screenshot);

    screenshotContentContainer.append(screenshotResolutionName);
    screenshotContentContainer.append(screenshotContainer);

    return screenshotContentContainer;
}

function enlargeScreenshot(event) {
    let screenshot = event.target;
    let screenshotSrc = screenshot.src;
    let deviceType = screenshot.parentElement.dataset.deviceType;

    screenshotInspector.setImage(screenshotSrc, deviceType);
    screenshotInspector.show();
}

export {takeScreenshot}