import resolutions from '../../resolutionsJson.js'

const selectAllResolutionsButton = document.querySelector('.select-all-button');
const deselectAllResolutionsButton = document.querySelector('.deselect-all-button');

/**
 * Generates all resolutions in the resolutions json as htmlElements
 */
for (let resolutionJson in resolutions) {
    let resolutionData = resolutions[resolutionJson];
    generateResolutionElement(resolutionData, resolutionJson)
}

/**
 * Adds a resolution checkbox to the resolution list
 *
 * @param {Object.<string, resolution{width, height}, isCheckedOnLoad>} resolutionData
 * @param key
 */
function generateResolutionElement(resolutionData, key) {
    let resolutionElement = document.createElement('label');
    resolutionElement.classList.add('resolution-container');

    let resolutionCheckbox = document.createElement('input');
    resolutionCheckbox.type = 'checkbox';
    resolutionCheckbox.name = key;
    resolutionCheckbox.checked = true;

    resolutionElement.appendChild(resolutionCheckbox);
    resolutionElement.append(resolutionData.deviceName);
    resolutionList.appendChild(resolutionElement)
}

const resolutionArray = Array.from(resolutionList.children);

/**
 * Event listeners to select/deselect all resolutions
 */
selectAllResolutionsButton.addEventListener('click', () => {
    resolutionArray.forEach((resolutionElement) => {
        resolutionElement.firstChild.checked = true;
    })
});

deselectAllResolutionsButton.addEventListener('click', () => {
    resolutionArray.forEach((resolutionElement) => {
        resolutionElement.firstChild.checked = false;
    })
});